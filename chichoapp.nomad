job "chichoapp" {
  datacenters = ["dc1"]

  group "appserver" {
    count = 1
    task "server" {
      driver = "docker"

      config {
        image = "yonsy/chichoapp:latest"
        port_map {
          frontendPort = 3000
        }
      }

      resources {
        cpu = 500
        memory = 256

        network {
          mbits = 10
          port "frontendPort" {
            static = "3000"
          }
        }
      }
    }
  }
}
