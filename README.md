# DevOps Challenge

Prueba diseñada para ver tus habilidades en el mundo DevOps. Se evaluará las herramientas fundamentales que utilizamos tales como docker, Nomad/Consul, CI/CD y Terraform.

¡Te deseamos mucho éxito!


## Tareas
* Dockerizar la aplicacion node
* Crear un pipleine para construir la imagen.
* Crear un Nomad job para desplegar la imagen en Nomad
* Crear un archivo Terraform para lanzar una instancia en EC2 con estas caracteristicas:
  * Instancia **t3a.nano**
  * Disco con **20GB**
  * Acceso SSH a la IP **11.22.33.44**
  * Imagen base **Amazon Linux 2**
* Documentar las acciones realizadas

**Importante**
> Documentar correctamente el proceso es muy importante para asegurar que el equipo pueda continuar con las tareas de ser necesario.

## Bonus
* Imagen docker pequeña
* Ejecutar con un usuario distinto a root
* Que el pipeline ejecute los tests antes de hacer build
* Usar Forever o PM2

## Dockerizar la aplicacion y crear un pipeline para construir la imagen

- crear el archivo `bitbucket-pipelines.yml` con el service `docker` para el build step y los pasos iniciales para instalar y correr los tests en la nodejs app (corregir bugs y fallas que se declaren).
- activar el pipeline en Bitbucket.
- corrida el primer pipeline as sucess (build and test) pasamos a definir al usuario y pwd (access token) para acceder, en este caso a mi personal registry en docker hub.
- agregar los pasos para build y push al docker hub (https://hub.docker.com/repository/docker/yonsy/chichoapp)

## Crear un Nomad job para desplegar la imagen en Nomad

- `chichoapp.nomad`

## Crear Terraform files para crear instancia EC2 con los requerimientos indicados.

- ubicados en el directorio `terraform/`
- crear previamente:
  - VPC
  - subred publica, internet gateway y tablas de ruteo
  - data resource describiendo una Amazon Linux 2 AMI
  - algunas reglas adicionales para definir tantas subredes como availability zones activas se tengan.
  - security group para permitir el acceso a la IP designada
- crear instancia EC2 con las caracteristicas requeridas

OBSERVACIONES:
- Lo del Nomad Job se basa solo en lo que conozco del mismo, lo que yo he experimentado es mas que nada versiones locales corriendo con LXC containers (y con LXD containers, el proyecto que comenzo como Nomad-LXD, termino siendo LXDock)
- Bitbucket no maneja un Docker Registry propio como Github y Gitlab, asi que estoy usando un public repository en Docker Hub para subir la imagen (https://hub.docker.com/repository/docker/yonsy/chichoapp). Se referencia a la misma en el Nomad Job al definir el task.
- Se ha incluido la creacion y destruccion de la VPC, subred publica y la instancia EC2 en el pipeline de Bitbucket para mostrar su funcionalidad completa.
- Para correr la construccion de la VPC. subredes e instancia EC2 manualmente, se deben definir tres env vars:
  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY
  - AWS_DEFAULT_REGION
  que son el access key id, el secret access key y la region (datacenter) de la cuenta AWS IAM con permisos para la creacion de VPC, Subredes, Security Policies, Instancias EC2 y Volumenes
- no se ha creado ningun key para acceder a la instancia por SSH solo se ha designado el acceso a la IP requerida.
- se han agregado outputs en terraform para que se puedan ver en los logs de los pipelines los resultados
