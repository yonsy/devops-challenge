FROM node:16-alpine

WORKDIR /app

COPY ./package.json /app
COPY ./package-lock.json /app
COPY ./index.js /app
COPY ./utils app
RUN npm install pm2 -g
RUN npm ci
EXPOSE 3000
USER node
CMD ["pm2-runtime", "index.js"]
