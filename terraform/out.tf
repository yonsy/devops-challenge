output vpc_id {
  value = aws_vpc.second.id
}

output vpc_cidr {
  value = aws_vpc.second.cidr_block
}

output ec2_public_dns {
  value = aws_instance.public.public_dns
}

output ec2_public_ip {
  value = aws_instance.public.public_ip
}
