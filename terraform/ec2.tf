resource "aws_security_group" "sg_ec2" {
  name = "second rules"
  vpc_id = aws_vpc.second.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "11.22.33.44/32"
    ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
}

resource "aws_instance" "public" {

  ami                     = data.aws_ami.amazon-linux-2.id
  instance_type           = "t3a.nano"
  subnet_id               = aws_subnet.public[0].id
  vpc_security_group_ids  = [aws_security_group.sg_ec2.id]

  root_block_device {
    delete_on_termination = true
    volume_size = 20
    volume_type = "gp2"
  }

}
